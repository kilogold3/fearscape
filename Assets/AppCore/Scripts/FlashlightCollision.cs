﻿using UnityEngine;

public class FlashlightCollision : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "LionStatue")
        {
            other.GetComponent<EnemyPathfindingFacade>().SetActivePathfinding(false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "LionStatue")
        {
            other.GetComponent<EnemyPathfindingFacade>().SetActivePathfinding(true);
        }
    }
}
