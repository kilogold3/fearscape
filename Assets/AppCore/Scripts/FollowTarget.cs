﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour
{
    public Transform target;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        NavMeshAgent ag = GetComponent<NavMeshAgent>();
        ag.SetDestination(target.position);
    }
}
