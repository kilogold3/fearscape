﻿using UnityEngine;
using System.Collections;

public class ArmInputController : MonoBehaviour
{
    [Range(0,90)]
    public float armMaxRot;

    public float angularSpeed;

    private float initialRot;

    // Use this for initialization
    void Start()
    {
        initialRot = transform.localEulerAngles.x;
    }

    // Update is called once per frame
    void Update()
    {
        float armAxisDelta;
#if UNITY_ANDROID && UNITY_EDITOR
        armAxisDelta = -Input.GetAxis("Mouse Y");
#elif UNITY_ANDROID && !UNITY_EDITOR
        Vector2 secondaryAxis = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
        armAxisDelta = -secondaryAxis.y;
#endif
        var curLocalArmRot = transform.localEulerAngles;
        curLocalArmRot.x += (armAxisDelta * angularSpeed * Time.deltaTime);

        //Special kind of clamping due to 180/360 degree conversions.
        var deltaAngle = curLocalArmRot.x - initialRot;
        if (deltaAngle > 180) deltaAngle -= 360;
        if (deltaAngle < -180) deltaAngle += 360;
        if (deltaAngle < -armMaxRot) { curLocalArmRot.x = -armMaxRot; }
        if (deltaAngle > armMaxRot) { curLocalArmRot.x = armMaxRot; }

        transform.localEulerAngles = curLocalArmRot;
    }
}
