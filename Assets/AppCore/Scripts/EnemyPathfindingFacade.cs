﻿using UnityEngine;
using System.Collections;

public class EnemyPathfindingFacade : MonoBehaviour
{
    public void SetActivePathfinding(bool isActive)
    {
        GetComponent<FollowTarget>().enabled = isActive;
        GetComponent<NavMeshAgent>().enabled = isActive;
    }
}
