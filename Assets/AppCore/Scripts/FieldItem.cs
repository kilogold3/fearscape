﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

[RequireComponent(typeof(VRInteractiveItem))]
public class FieldItem : MonoBehaviour
{
    public SelectionRadial selectionRadial;

    // Use this for initialization
    void Start()
    {
        var interaction = GetComponent<VRInteractiveItem>();
        interaction.OnOver += OnOver;
        interaction.OnOut += OnOut;
        selectionRadial.OnSelectionComplete += OnSelectionComplete;
    }

    private void OnSelectionComplete()
    {
        Destroy(gameObject);
        OnOut();
    }

    void OnOver()
    {
        selectionRadial.Show();
    }

    void OnOut()
    {
        selectionRadial.Hide();
    }
}
